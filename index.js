/*
    Create functions which can manipulate our arrays.
*/

let registeredUsers = [
    "James Jeffries",
    "Gunther Smith",
    "Macie West",
    "Michelle Queen",
    "Shane Miguelito",
    "Fernando Dela Cruz",
    "Akiko Yukihime"
];

let friendsList = [];

/*
   1. Create a function which will allow us to register into the registeredUsers list.
        - this function should be able to receive a string.
        - determine if the input username already exists in our registeredUsers array.
            -if it is, show an alert window with the following message:
                "Registration failed. Username already exists!"
            -if it is not, add the new username into the registeredUsers array and show an alert:
                "Thank you for registering!"
        - invoke and register a new user.
        - outside the function log the registeredUsers array.
*/
    //hint:
    //use includes() method to check existing user
        //condition (if else) if exists-reg fail, else - ty for registering .


function addUser(username){
    let existingUser = registeredUsers.includes("username");
        if (existingUser) {
            alert("Registration failed. Username already exists!")
        } else  {
            registeredUsers.push(username)
            alert("Thank you for registering!")
        }
}


/*   let existingUser = registeredUsers.includes("James Jeffries");
   console.log(existingUser);*/


/*
    2. Create a function which will allow us to add a registered user into our friends list.
        - this function should be able to receive a string.
        - determine if the input username exists in our registeredUsers array.
            - if it is, add the foundUser in our friendList array.
                    -Then show an alert with the following message:
                        - "You have added <registeredUser> as a friend!"
            - if it is not, show an alert window with the following message:
                - "User not found."
        - invoke the function and add a registered user in your friendsList.
        - Outside the function log the friendsList array in the console.
*/
    //hints:
        //function addfrend(username){
            //use include method

            //condition - add push method if exsting, else, alert user not found
       // }

function addFriend(username){
        if (registeredUsers.includes(username)) {
            alert("You have added "+ username + " as a friend!")
            friendsList.push(username)
        } else  {
            alert("User not found.")
        }
}


/*
    3. Create a function which will allow us to show/display the items in the friendList one by one on our console.
        - If the friendsList is empty show an alert: 
            - "You currently have 0 friends. Add one first."
        - Invoke the function.
*///condition
            //forEach() to print out friends
    function displayFriends(){
        if (friendsList.length < 1) {
            alert("You currently have 0 friends. Add one first.")
        } else  {
            friendsList.forEach(function(friends){
                console.log(friends)
            })
        }
    }


/*
    4. Create a function which will display the amount of registered users in your friendsList.
        - If the friendsList is empty show an alert:
            - "You currently have 0 friends. Add one first."
        - If the friendsList is not empty show an alert:
            - "You currently have <numberOfFriends> friends."
        - Invoke the function
*/
    function displayAmount(){
        if (friendsList.length < 1) {
                    alert("You currently have 0 friends. Add one first.")
                } else  {
                    alert("You currently have " + friendsList.length + " friend/s.")
                }
    }


/*
    5. Create a function which will delete the last registeredUser you have added in the friendsList.
        - If the friendsList is empty show an alert:
            - "You currently have 0 friends. Add one first."
        - Invoke the function.
        - Outside the function log the friendsList array.
*/
    function deleteFriend(){
        if (friendsList.length < 1) {
                    alert("You currently have 0 friends. Add one first.")
                } else  {
                    friendsList.pop();
                    alert("Successfully deleted!")
                }
    }


/*
    Stretch Goal:

    Instead of only deleting the last registered user in the friendsList delete a specific user instead.
        -You may get the user's index.
        -Then delete the specific user with splice().
*/

function deleteSpecificFriend(findUser){
        if (friendsList.length < 1) {
                    alert("You currently have 0 friends. Add one first.")
                } else  {
                   
                   let index = friendsList.indexOf(findUser);
                   if (index > -1) {
                     friendsList.splice(index, 1);
                }
                alert("Successfully deleted a friend!")
                }
    
}


